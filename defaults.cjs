function defaults(obj,defaults){
    if(typeof obj === 'undefined'){
        return defaults;
    }else{
        if(obj instanceof Number || obj instanceof String || obj instanceof Boolean || obj instanceof BigInt || obj instanceof Array || obj instanceof Symbol){
            return obj;
        }else if(typeof obj == 'number' || typeof obj== 'boolean' || typeof obj == 'string' || typeof obj == 'symbol' ||
        typeof obj == 'bigint' || typeof obj == 'function'){
            return obj;
        }else{
            for(let key in defaults){
                if(typeof obj[key] == 'undefined'){
                    obj[key] = defaults[key];
                }
            }
        }
        return obj;
    }
}

module.exports = defaults;