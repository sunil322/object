function pairs(obj){
    let pairSet=[];

    if(typeof obj != 'undefined'){
      if(obj instanceof Number || obj instanceof String || obj instanceof Boolean || obj instanceof BigInt || obj instanceof Array || obj instanceof Symbol){
        return pairSet;
      }else if(typeof obj == 'number' || typeof obj== 'boolean' || typeof obj == 'string' || typeof obj == 'symbol' ||
      typeof obj == 'bigint' || typeof obj == 'function'){
        return pairSet;
      }else{
        for(let key in obj){
            let entry=[];
            entry.push(key);
            entry.push(obj[key]);
            pairSet.push(entry);
        }
        return pairSet;
      }
    }else{
        return pairSet;
    }
}

module.exports = pairs;