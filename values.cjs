function values(obj){
    let valueSet = [];
    if(typeof obj != 'undefined'){
        if(obj instanceof Number || obj instanceof String || obj instanceof Boolean || obj instanceof BigInt || obj instanceof Array || obj instanceof Symbol){
          return valueSet;
        }else if(typeof obj == 'number' || typeof obj== 'boolean' || typeof obj == 'string' || typeof obj == 'symbol' ||
        typeof obj == 'bigint' || typeof obj == 'function'){
          return valueSet;
        }else{
          for(let key in obj){
              valueSet.push(obj[key]);
            }
        return valueSet;
        }
      }else{
        return valueSet;
    }
}

module.exports = values;