function invert(obj){
    let pairSet=[];

    if(typeof obj != 'undefined'){
      if(obj instanceof Number || obj instanceof String || obj instanceof Boolean || obj instanceof BigInt || obj instanceof Array || obj instanceof Symbol){
        return pairSet;
      }else if(typeof obj == 'number' || typeof obj== 'boolean' || typeof obj == 'string' || typeof obj == 'symbol' ||
      typeof obj == 'bigint' || typeof obj == 'function'){
        return pairSet;
      }else{
        let invertedObject= {};
        for(let key in obj){
            invertedObject[obj[key]] = key; 
        }
        return invertedObject;
      }
    }else{
        return pairSet;
    }
}

module.exports = invert;