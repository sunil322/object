function keys(obj) {
  let keySet = [];

  if (typeof obj != 'undefined' || typeof obj === 'object') {
      for (let key in obj) {
        keySet.push(key);
      }
      return keySet;
    }
  else {
    return keySet;
  }
}

module.exports = keys;
