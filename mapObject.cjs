function mapObject(obj,mapFunc){
    if(typeof obj!=='undefined'){
        if(typeof mapFunc === 'function'){
            if(obj instanceof Number || obj instanceof String || obj instanceof Boolean || obj instanceof BigInt || obj instanceof Array || obj instanceof Symbol){
                return {};
            }else if(typeof obj == 'number' || typeof obj== 'boolean' || typeof obj == 'string' || typeof obj == 'symbol' ||
              typeof obj == 'bigint' || typeof obj == 'function'){
                return {};
            }else{
                for(let key in obj){
                    obj[key] = mapFunc(obj[key]);
                }
              return obj;
            }
        }else{
            return obj;
        }
    }else{
        return {};
    }
    
}

module.exports = mapObject;